import { Component, OnInit } from '@angular/core';
import { HeaderItem } from '../models/header-item';
import { MenuService } from '../service/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerItems: HeaderItem[];

  constructor(private headerService: MenuService) { }

  ngOnInit(): void {
    this.headerService.getHeaderItems().subscribe(data => {
      this.headerItems = data;
    });
  }

  onMakeActive(header: HeaderItem) {
    this.headerItems.forEach(h => h.active=false);
    header.active = true;
  }

}
