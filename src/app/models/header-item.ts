export class HeaderItem {
    name: string;
    link: string;
    order?: number;
    image?: string;
    fontAwesome?: string;
    background?: string;
    active: boolean;
}
