import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HeaderItem } from '../models/header-item';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor() { }
  
  getHeaderItems(): Observable<HeaderItem[]> {
    let headerItems:HeaderItem[] = [
      {
        name: "profile",
        image: "",
        link: "profile",
        background: "black",
        active:false
      },
      {
        name: "heirachy",
        fontAwesome: "sitemap",
        link: "profile",
        background: "black",
        active:false
      },
      {
        name: "add",
        fontAwesome: "plus",
        link: "profile",
        background: "black",
        active:false
      }
    ] 
    return of(headerItems);
  }
}
